QT += testlib
QT += gui core
QT += widgets
CONFIG += qt warn_on depend_includepath testcase

TEMPLATE = app

SOURCES +=  tst_unit_test.cpp \
    soundmixer.cpp \
    AudioFile.cpp \
    mainwindow.cpp

HEADERS += \
    soundmixer.h \
    AudioFile.h \
    mainwindow.h

FORMS += \
    mainwindow.ui
