#include "soundmixer.h"
#include <stdio.h>

SoundMixer::SoundMixer(QObject *parent) :
    QObject(parent)
{

}

SoundMixer::~SoundMixer()
{

}

QString SoundMixer::openFile(QString testFileConst)
{
    fileName = testFileConst.isEmpty()
      ? QFileDialog::getOpenFileName()
      : testFileConst;

    return fileName;
}

QString SoundMixer::openSecondFile(QString testFileConst)
{
    fileName2 = testFileConst.isEmpty()
      ? QFileDialog::getOpenFileName()
      : testFileConst;

    return fileName2;
}

bool SoundMixer::loadAudioFile()
{
    return audiofile.load(fileName.toStdString());
}

bool SoundMixer::loadSecondAudioFile()
{
    return audiofile2.load(fileName2.toStdString());
}

bool SoundMixer::openWAVFile()
{
    wavfile.setFileName(fileName);

    return (wavfile.open(QIODevice::ReadOnly));
}

bool SoundMixer::openSecondWAVFile()
{
    wavfile2.setFileName(fileName2);

    return (wavfile2.open(QIODevice::ReadOnly));
}

void SoundMixer::closeWAVFile()
{
    wavfile.close();
}

bool SoundMixer::saveMixFile(QString mixFileName)
{
    return audiofile.save(mixFileName.toStdString(),AudioFileFormat::Wave);
}

void SoundMixer::makeMix()
{
    AudioFile<double>::AudioBuffer buffer;
    buffer.resize (2);

    buffer[0].resize (100000);
    buffer[1].resize (100000);

    if (audiofile.isStereo())
       {
            for (int i = 0; i < audiofile.getNumChannels(); i++)
            {
                buffer[i].resize (audiofile.samples[i].size());
                for (int j = 0; j < audiofile.samples[i].size(); j++)
                {buffer[i][j] = audiofile.samples[i][j];}
            }
        }
    if (audiofile.isMono())
       {
            buffer[0].resize (audiofile.samples[0].size());
            buffer[1].resize (audiofile.samples[0].size());
            for (int j = 0; j < audiofile.samples[0].size(); j++)
               { buffer[0][j] = audiofile.samples[0][j];
                buffer[1][j] = audiofile.samples[0][j];}
        }

    if (audiofile2.isStereo())
       { for (int i = 0; i < audiofile2.getNumChannels(); i++)
            {   buffer[i].resize (buffer[i].size()+audiofile2.samples[i].size());
                for (int j = 0; j < audiofile2.samples[i].size(); j++)
                {buffer[i][j+audiofile.samples[i].size()] = audiofile2.samples[i][j];}
            }
        }
    if (audiofile2.isMono())
       {
            buffer[0].resize (buffer[0].size()+audiofile2.samples[0].size());
            buffer[1].resize (buffer[1].size()+audiofile2.samples[0].size());
            for (int j = 0; j < audiofile2.samples[0].size(); j++)
               { buffer[0][j+audiofile.samples[0].size()] = audiofile2.samples[0][j];
                 buffer[1][j+audiofile.samples[0].size()] = audiofile2.samples[0][j];}
        }

    bool ok = audiofile.setAudioBuffer (buffer);
    audiofile.setAudioBufferSize (2, buffer[1].size());

    saveMixFile("D:/2/mix2.wav");
}
