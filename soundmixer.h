#ifndef SOUNDMIXER_H
#define SOUNDMIXER_H

#include <QObject>
#include <QFileDialog>
#include <QFile>
#include "AudioFile.h"

class SoundMixer : public QObject
{
    Q_OBJECT
public:
    explicit SoundMixer(QObject *parent = nullptr);
    QString openFile(QString testFileConst = "");
    QString openSecondFile(QString testFileConst = "");
    bool saveMixFile(QString mixFileName = "");
    bool openWAVFile();
    bool openSecondWAVFile();
    void closeWAVFile();
    QString fileName;
    QString fileName2;
    QFile wavfile;
    QFile wavfile2;
    bool loadAudioFile();
    bool loadSecondAudioFile();
    AudioFile<double> audiofile;
    AudioFile<double> audiofile2;
    void makeMix();
    ~SoundMixer();
signals:

public slots:
};

#endif // SOUNDMIXER_H
