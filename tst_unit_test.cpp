#include <QtTest>
#include "soundmixer.h"
// add necessary includes here


class unit_test : public QObject
{
    Q_OBJECT

public:
    unit_test();
    ~unit_test();

private slots:
    void object_exist();
    void open_file();
    void open_file2();
    void close_file();
    void check_file();
    void open_wavefile();
    void audiofile_load();
    void test_wav_stereo_16bit_44100();
    void test_wav_mono_16bit_44100();
    void test_wav_stereo_8bit_44100();
    void test_wav_stereo_16bit_48000();
    void open_second_file();
    void save_first_file();
};

unit_test::unit_test()
{

}

unit_test::~unit_test()
{

}

void unit_test::object_exist()
{
    SoundMixer *soundMixer = new SoundMixer();
    QVERIFY2(soundMixer, "SoundMixer created?");
}

void unit_test::audiofile_load()
{
    SoundMixer *soundMixer = new SoundMixer();
    soundMixer->openFile("D:/2/wav_stereo_16bit_44100.wav");
    QVERIFY2(soundMixer->loadAudioFile(), "audiofile created?");
}

void unit_test::open_file()
{
    SoundMixer *soundMixer = new SoundMixer();
    QVERIFY2(!soundMixer->openFile("D:/2/wav_stereo_16bit_44100.wav").isNull(), "File opened?");
}

void unit_test::open_second_file()
{
    SoundMixer *soundMixer = new SoundMixer();
    soundMixer->openSecondFile("D:/2/wav_stereo_8bit_44100.wav");
    soundMixer->openSecondWAVFile();
    soundMixer->loadSecondAudioFile();
    QCOMPARE(soundMixer->audiofile2.getBitDepth(),8);
    QCOMPARE(soundMixer->audiofile2.getSampleRate(),44100);
    QVERIFY2(soundMixer->audiofile2.isStereo(),"D:/2/wav_stereo_8bit_44100.wav is stereo?");
    QCOMPARE(soundMixer->audiofile2.getNumChannels(),2);
}

void unit_test::close_file()
{
    SoundMixer *soundMixer = new SoundMixer();
    QVERIFY2(!soundMixer->openFile("D:/2/wav_stereo_16bit_44100.wav").isNull(), "File opened?");
    soundMixer->openWAVFile();
    QCOMPARE(soundMixer->wavfile.fileName().toStdString(),"D:/2/wav_stereo_16bit_44100.wav");
    soundMixer->closeWAVFile();
    QVERIFY2(!soundMixer->wavfile.isOpen(), "File opened?");

}

void unit_test::open_file2()
{
    SoundMixer *soundMixer = new SoundMixer();
    QVERIFY2(!soundMixer->openFile("D:/2/wav_stereo_16bit_44100.wav").isNull(), "File opened?");
    soundMixer->openWAVFile();
    QCOMPARE(soundMixer->wavfile.fileName().toStdString(),"D:/2/wav_stereo_16bit_44100.wav");
}

void unit_test::open_wavefile()
{
    SoundMixer *soundMixer = new SoundMixer();
    soundMixer->openFile("D:/2/wav_stereo_16bit_48000.wav");
    soundMixer->openWAVFile();
    QVERIFY2(soundMixer->wavfile.isOpen(), "Wavfile opened?");
    QCOMPARE(soundMixer->wavfile.fileName(),soundMixer->fileName);
    QVERIFY2(soundMixer->wavfile.isReadable(), "Wavfile is readable?");
}


void unit_test::check_file()
{
    SoundMixer *soundMixer = new SoundMixer();
    //мы открыли и создали файл?
    QCOMPARE(soundMixer->openFile("D:/2/wav_stereo_16bit_44100.wav"),soundMixer->fileName);

}

void unit_test::test_wav_stereo_16bit_44100()
{
    SoundMixer *soundMixer = new SoundMixer();
    soundMixer->openFile("D:/2/wav_stereo_16bit_44100.wav");
    soundMixer->openWAVFile();
    soundMixer->loadAudioFile();
    QCOMPARE(soundMixer->audiofile.getBitDepth(),16);
    QCOMPARE(soundMixer->audiofile.getSampleRate(),44100);
    QVERIFY2(soundMixer->audiofile.isStereo(),"D:/2/wav_stereo_16bit_44100.wav is stereo?");
    QCOMPARE(soundMixer->audiofile.getNumChannels(),2);
}

void unit_test::test_wav_mono_16bit_44100()
{
    SoundMixer *soundMixer = new SoundMixer();
    soundMixer->openFile("D:/2/wav_mono_16bit_44100.wav");
    soundMixer->openWAVFile();
    soundMixer->loadAudioFile();
    QCOMPARE(soundMixer->audiofile.getBitDepth(),16);
    QCOMPARE(soundMixer->audiofile.getSampleRate(),44100);
    QVERIFY2(soundMixer->audiofile.isMono(),"D:/2/wav_stereo_16bit_44100.wav is stereo?");
    QCOMPARE(soundMixer->audiofile.getNumChannels(),1);
    delete soundMixer;
}

void unit_test::test_wav_stereo_16bit_48000()
{
    SoundMixer *soundMixer = new SoundMixer();
    soundMixer->openFile("D:/2/wav_stereo_16bit_48000.wav");
    soundMixer->openWAVFile();
    soundMixer->loadAudioFile();
    QCOMPARE(soundMixer->audiofile.getBitDepth(),16);
    QCOMPARE(soundMixer->audiofile.getSampleRate(),48000);
    QVERIFY2(soundMixer->audiofile.isStereo(),"D:/2/wav_stereo_16bit_48000.wav is stereo?");
    QCOMPARE(soundMixer->audiofile.getNumChannels(),2);
}

void unit_test::test_wav_stereo_8bit_44100()
{
    SoundMixer *soundMixer = new SoundMixer();
    soundMixer->openFile("D:/2/wav_stereo_8bit_44100.wav");
    soundMixer->openWAVFile();
    soundMixer->loadAudioFile();
    QCOMPARE(soundMixer->audiofile.getBitDepth(),8);
    QCOMPARE(soundMixer->audiofile.getSampleRate(),44100);
    QVERIFY2(soundMixer->audiofile.isStereo(),"D:/2/wav_stereo_8bit_44100.wav is stereo?");
    QCOMPARE(soundMixer->audiofile.getNumChannels(),2);
}

void unit_test::save_first_file()
{
    SoundMixer *soundMixer = new SoundMixer();
<<<<<<< tst_unit_test.cpp
    soundMixer->openFile("D:/2/wav_stereo_16bit_44100.wav");
    soundMixer->openWAVFile();
    soundMixer->loadAudioFile();
    QCOMPARE(soundMixer->audiofile.getBitDepth(),16);
    QCOMPARE(soundMixer->audiofile.getSampleRate(),44100);
    QVERIFY2(soundMixer->audiofile.isStereo(),"D:/2/wav_stereo_16bit_44100.wav is stereo?");
    QCOMPARE(soundMixer->audiofile.getNumChannels(),2);
    //QVERIFY2(soundMixer->saveMixFile("D:/2/mix.wav"),"D:/2/mix.wav is save?");

    soundMixer->openSecondFile("D:/2/wav_mono_16bit_44100.wav");
    soundMixer->openSecondWAVFile();
    soundMixer->loadSecondAudioFile();
    QCOMPARE(soundMixer->audiofile2.getBitDepth(),16);
    QCOMPARE(soundMixer->audiofile2.getSampleRate(),44100);
    QVERIFY2(soundMixer->audiofile2.isMono(),"D:/2/wav_mono_16bit_44100.wav is mono?");
    QCOMPARE(soundMixer->audiofile2.getNumChannels(),1);

    soundMixer->makeMix();
=======
    soundMixer->openFile("D:/2/wav_stereo_8bit_44100.wav");
    soundMixer->openWAVFile();
    soundMixer->loadAudioFile();
    QCOMPARE(soundMixer->audiofile.getBitDepth(),8);
    QCOMPARE(soundMixer->audiofile.getSampleRate(),44100);
    QVERIFY2(soundMixer->audiofile.isStereo(),"D:/2/wav_stereo_8bit_44100.wav is stereo?");
    QCOMPARE(soundMixer->audiofile.getNumChannels(),2);
    soundMixer->saveMixFile("D:/2/mix.wav");

    soundMixer->openSecondFile("D:/2/mix.wav");
    soundMixer->openSecondWAVFile();
    soundMixer->loadSecondAudioFile();
    QCOMPARE(soundMixer->audiofile2.getBitDepth(),8);
    QCOMPARE(soundMixer->audiofile2.getSampleRate(),44100);
    QVERIFY2(soundMixer->audiofile2.isStereo(),"D:/2/mix.wav is stereo?");
    QCOMPARE(soundMixer->audiofile2.getNumChannels(),2);
>>>>>>> tst_unit_test.cpp
}


QTEST_APPLESS_MAIN(unit_test)

#include "tst_unit_test.moc"
